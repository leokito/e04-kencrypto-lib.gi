export interface CoinData {
        [key: string]: {
            id: number;
            name: string;
            slug: string;
            symbol: string;
            date_added: Date;
            last_updated: Date;
            quote: QuoteData;
    }
}

export interface CoinMarketData {
    data: CoinData;
}

export interface QuoteData {
    [key: string]: {
        price: number,
        last_updated: Date,
    }
}

export interface Conversion {
    id: number;
    symbol: string;
    name: string;
    amount: number;
    last_updated: Date;
    quote: QuoteData;
}

export interface InvalidData {
    status: {
        error_message: string | null;
    }
}
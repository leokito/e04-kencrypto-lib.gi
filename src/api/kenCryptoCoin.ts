import axios, { AxiosInstance, AxiosResponse } from "axios";
import { CoinMarketData, Conversion, InvalidData } from "../types/interfaces";

export default class CoinMarketCap {
    
    baseURL: string = 'https://pro-api.coinmarketcap.com/';
    axiosInstance: AxiosInstance;

    constructor() {
        this.axiosInstance = axios.create({
            baseURL: this.baseURL
        });
    }
    
    async quotes(symbol: Array<string>) {
        const requestURL = `v1/cryptocurrency/quotes/latest?symbol=${symbol}`;
        try {
            const response: AxiosResponse<any, any> = await this.axiosInstance.get(requestURL, {
                headers: {
                    "X-CMC_PRO_API_KEY": "7f5c8a18-21dd-41e1-b202-a2c55323beea",
                }
            });
            return response.data.data as CoinMarketData

        } catch (e) {
            if (axios.isAxiosError(e)) {
              return e.response?.data as InvalidData;
            }
          }
    }

    async getConversion(symbol: Array<string>, amount: number, convert: Array<string>) {

        const requestURL = `v1/tools/price-conversion?symbol=${symbol}&amount=${amount}&convert=${convert}`;

        try {
            const response: AxiosResponse<any, any> = await this.axiosInstance.get(requestURL, {
                headers: {
                    "X-CMC_PRO_API_KEY":"7f5c8a18-21dd-41e1-b202-a2c55323beea",
                }
            });
            return response.data.data as Conversion;
        } catch (e) {
            if (axios.isAxiosError(e)) {
              return e.response?.data as InvalidData;
            }
          }
    }
}